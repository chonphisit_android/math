package buu.chonphisit.gamecalculator

import android.app.Activity
import android.graphics.Color
import android.graphics.Color.parseColor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.chonphisit.gamecalculator.databinding.ActivityMainBinding
import buu.chonphisit.gamecalculator.databinding.ActivityPlusBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class PlusActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPlusBinding
    private val score:Score = Score()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plus)

        start()
        binding.score = score


    }

    private fun start() {
        binding.apply {
            val randomnum = Random.nextInt(0, 10)
            numtext1.text = randomnum.toString()

            val randomnum1 = Random.nextInt(0, 10)
            numtext2.text = randomnum1.toString()

            val answer = (randomnum + randomnum1)
            val choice = Random.nextInt(0, 3)

            if (choice == 0) {
                btn1.text = answer.toString()
                btn2.text = (answer + 1).toString()
                btn3.text = (answer - 1).toString()
            } else if (choice == 1) {
                btn1.text = (answer + 1).toString()
                btn2.text = answer.toString()
                btn3.text = (answer - 1).toString()
            } else {
                btn1.text = (answer - 1).toString()
                btn2.text = (answer + 1).toString()
                btn3.text = answer.toString()

            }

            val correct: Int = 0
            val incorrect: Int = 0


            btn1.setOnClickListener {
                if (btn1.text.toString().toInt() == answer) {
                    ansCorrect(textalert)
                    anscorrect(correct)
                    start()
                } else {
                    ansIncorrect(textalert)
                    ansincorect(incorrect)
                    start()
                }
            }
            btn2.setOnClickListener {
                if (btn2.text.toString().toInt() == answer) {
                    ansCorrect(textalert)
                    anscorrect(correct)
                    start()
                } else {
                    ansIncorrect(textalert)
                    ansincorect(incorrect)
                    start()
                }
            }
            btn3.setOnClickListener {
                if (btn3.text.toString().toInt() == answer) {
                    ansCorrect(textalert)
                    anscorrect(correct)
                    start()
                } else {
                    ansIncorrect(textalert)
                    ansincorect(incorrect)
                    start()
                }
            }
        }

    }


    private fun ansincorect(incorrect: Int) {
//        pointIncorrect.text = (pointIncorrect.text.toString().toInt()+1).toString()
        score?.myincorrect = (score.myincorrect.toInt()+1).toString()
        binding.invalidateAll()
    }

    private fun anscorrect(correct: Int) {
//        pointCorrect.text = (pointCorrect.text.toString().toInt()+1).toString()
        score?.mycorrect = (score.mycorrect.toInt()+1).toString()
        binding.invalidateAll()
    }

    private fun ansIncorrect(textalert: TextView?) {
        textalert!!.text = ("Incorrect")
        textalert.setTextColor(Color.RED)
    }

    private fun ansCorrect(textalert: TextView?) {
        textalert!!.text = ("Correct")
        textalert.setTextColor(Color.GREEN)
    }
}